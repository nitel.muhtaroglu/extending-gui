package cs102;

import javax.swing.*;
import javax.swing.border.EtchedBorder;

public class MyWindow extends JFrame {
    private JPanel mainPanel;
    private JButton button;
    private JTextField textField;
    private JLabel label;

    public MyWindow() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(400, 400);

        this.mainPanel = new JPanel();
        this.add(this.mainPanel);
        this.mainPanel.setLayout(null);

        this.button = new JButton("Button");
        this.button.setBounds(10, 10, 200, 60);
        this.mainPanel.add(this.button);

        this.textField = new JTextField("My size is manually set...");
        this.textField.setBounds(10, 90, 90, 40);
        this.mainPanel.add(this.textField);

        this.label = new JLabel("I am a label with a border.");
        this.label.setBounds(50, 120, 200, 80);
        this.mainPanel.add(this.label);
        this.label.setBorder(new EtchedBorder());

        button.addActionListener(new MyButtonHandler(this.textField));

        this.setVisible(true);
    }
}
