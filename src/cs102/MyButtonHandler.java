package cs102;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyButtonHandler implements ActionListener {
    private JTextField textField;

    public MyButtonHandler(JTextField textField) {
        this.textField = textField;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        this.textField.setText("Hello");
    }
}
