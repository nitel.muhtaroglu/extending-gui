package cs102;

import javax.swing.*;
import javax.swing.border.EtchedBorder;

public class Main {

    public static void main(String[] args) {
        JFrame frame;
        for (int i = 0; i < 5; ++i) {
            frame = new MyWindow();
            frame.setTitle("Window" + (i + 1));
            frame.setLocation(i * 100, 0);
        }
    }
}
